# SPACE ODDITY
#### Phaser 3 / ES6 / Webpack Random Space Game

![Space Oddity](http://spaceoddity.simonpelletier.net/assets/img/logo.jpg)

## Play

http://spaceoddity.simonpelletier.net

## Install

```
npm install
```

## Run Dev

```
npm run dev
```

## Build

```
npm run deploy
```


Thanks to nkholski (boilerplate)
https://github.com/nkholski/phaser3-es6-webpack